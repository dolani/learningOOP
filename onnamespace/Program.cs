﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vehicle;
using Planet;


namespace LearningOOP
{
    class Program
    {
        static void Main(string[] args)
        {
            Car Toyota = new Car();

            //cant be accessed outside
            //Console.WriteLine("Toyota has " + toyota.GetWheels() + " wheels");

            Console.WriteLine("Car color is : " + Toyota.GetColor());

            Console.WriteLine("Start the  Car: ");
            Toyota.GetStartCar();

            Console.WriteLine("Car started : " + Toyota.GetCarStatus());

            Console.WriteLine("Stop the  Car: ");

            Toyota.GetStopCar();

            Console.WriteLine("Car started : " + Toyota.GetCarStatus());

            Tricycle.CanMove();
            Console.ReadKey();

            Bicycle.NumberOfLegs(2);
            Console.ReadKey();

            Human manny = new Human();
            Console.WriteLine("Do you know humans are always more " + manny.GetTemper() + " tempered");
            Console.ReadKey();
            Animal.Dog();
            //Access denied Method is protected
            //manny.CanAbort();  
        }
    }

    class Conversion
    {
        public static void SecondsToMilliseconds(double number)
        {
            number = Convert.ToInt32(Console.ReadLine());
            double MS = number * 1000;
            Console.WriteLine("The equivalent in conversion is {0}", MS);
            Console.ReadLine();
        }

        public static void YearsToDays(int number)
        {
            number = Convert.ToInt32(Console.ReadLine());
            int days = number * 365;
            Console.WriteLine("The days equivalent is {0}", days);
            Console.ReadLine();
        }

        public static void YearsToHours()
        {
            int number;
            number = Convert.ToInt32(Console.ReadLine());
            int hours = number * 30;
            Console.WriteLine("The hours equivalent is {0}", hours);
            Console.ReadLine();
        }

        public static void FahrenheitToCelcius()
        {

            double F, C;

            Console.WriteLine("Enter your fahrenheit value that you want to convert");
            F = Convert.ToInt32(Console.ReadLine());
            C = ((F - 32) * 5 / 9);
            Console.WriteLine("The Celcius equivalent is {0}", C);
            Console.ReadLine();
        }
    }

    class Questionnaire
    {
        public void Continent()
        {
            Console.WriteLine("Which continent are you from?(AFR or ASIA or EUR )");
            string ans = Console.ReadLine();
            switch (ans)
            {
                case "AFR":
                    Console.WriteLine("you are from Africa");
                    Questionnaire.KnowStatus();
                    break;
                case "ASIA":
                    Console.WriteLine("you probably have a deem-like eyes");
                    Questionnaire.KnowStatus();
                    break;
                case "EUR":
                    Console.WriteLine("you are basically white");
                    Questionnaire.KnowStatus();
                    break;
                default:
                    Console.WriteLine("YOU ARE FROM ANOTHER PLANET");
                    Continent();
                    break;
            }
            Console.ReadKey();
        }

        public static void IsChange()
        {
            Console.WriteLine("Which continent would you have loved to come from?(AFR or ASIA or EUR )");
            string ans = Console.ReadLine();
            switch (ans)
            {
                case "AFR":
                    Console.WriteLine("Congrats, You are black");
                    break;
                case "ASIA":
                    Console.WriteLine("you now have a deem-like eyes");
                    break;
                case "EUR":
                    Console.WriteLine("you are white");
                    break;
                default:
                    Console.WriteLine("You are an Alien");
                    IsChange();
                    break;
            }
            Console.ReadKey();
        }

        public static void KnowStatus()
        {
            Console.WriteLine("Would you have loved to come from another continent? YES or NO");
            string ans = Console.ReadLine();
            switch (ans)
            {
                case "YES":
                    Questionnaire.IsChange();
                    break;
                case "NO":
                    Console.WriteLine("You sure are pround of yourself!!!..   press enter to exit");
                    break;
                default:
                    KnowStatus();
                    break;
            }
        }
    }
}

namespace Planet
{
    public class Human
    {
        public string GetTemper()
        {
            return "hot";
        }
        //cant be accessed outside here
        protected void CanAbort()
        {

            Console.WriteLine("and Only human can perform abortions");
            Console.ReadKey();
        }
    }

    public class Animal
    {
        public static void Dog()
        {
            //cannot be assessed because its protected
            // Human.CanAbort();

            Console.WriteLine("Can dogs abort?");

            Console.ReadKey();
        }

        protected bool CanBreathe = true;
        public bool StopBreathing()
        {
            CanBreathe = false;
            return CanBreathe;
        }

    }

    public class Inanimate
    {
        public static void GetState()
        {
            Console.WriteLine("I am always stationery");
            Console.ReadKey();
        }

        public bool CanMove = false;

        protected bool StartMovement()
        {
            CanMove = true;
            return CanMove;
        }
    }
}

namespace Vehicle
{

    public class Car
    {
        public int Wheels = 4;
        public string Color = "white";
        private bool IsStart = false;

        //wheels is public but cant be accessed outside this class 
        //because GetWheel method is made private
        private int GetWheels()
        {
            return Wheels;
        }

        public string GetColor()
        {
            return Color;
        }

        protected bool StartEngine()
        {
            IsStart = true;
            return IsStart;
        }
        protected bool StopEngine()
        {
            IsStart = false;
            return IsStart;
        }
        public void GetStartCar()
        {
            //is private but accessible in this class only
            StartEngine();
        }
        //Its public – so accessible outside class
        public void GetStopCar()
        {
            //"StopEngine()" is private but accessible and limited to this class only
            StopEngine();
        }
        //Its public – so accessible outside class
        public bool GetCarStatus()
        {
            return IsStart;
        }

    }

    public class Tricycle
    {
        public static void CanMove()
        {

            Bicycle.NumberOfLegs(3);

        }
    }

    public class Bicycle
    {
        public static void NumberOfLegs(int number)
        {

            Console.WriteLine("This vehicle has " + number + " legs");
        }
    }

}

